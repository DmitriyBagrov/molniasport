//
//  Article+CoreDataProperties.m
//  molniaSport
//
//  Created by Dmitriy Bagrov on 10.10.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Article+CoreDataProperties.h"

@implementation Article (CoreDataProperties)

@dynamic smallImageUrl;
@dynamic subtitle;
@dynamic title;
@dynamic uid;
@dynamic commentsCount;
@dynamic hits;
@dynamic imageUrl;
@dynamic content;

@end

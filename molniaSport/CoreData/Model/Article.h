//
//  Article.h
//  molniaSport
//
//  Created by Dmitriy Bagrov on 10.10.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Article : NSManagedObject

+ (NSDictionary *)mappingDictionary;
+ (NSArray *)identityAttributes;

@end

NS_ASSUME_NONNULL_END

#import "Article+CoreDataProperties.h"

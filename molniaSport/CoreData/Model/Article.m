//
//  Article.m
//  molniaSport
//
//  Created by Dmitriy Bagrov on 10.10.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//

#import "Article.h"

@implementation Article

+ (NSDictionary *)mappingDictionary {
    return @{@"id"            : @"uid",
             @"title"         : @"title",
             @"subtitle"      : @"subtitle",
             @"image_163_120" : @"smallImageUrl",
             @"content"        : @"content",
             @"hits"           : @"hits",
             @"comments_count" : @"commentsCount",
             @"image_330_215"  : @"imageUrl"};
}

+ (NSArray *)identityAttributes {
    return @[@"uid"];
}

@end

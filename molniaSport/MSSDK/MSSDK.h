//
//  MSSDK.h
//  molniaSport
//
//  Created by Dmitriy Bagrov on 09.10.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//

@interface MSSDK : NSObject

/**
 * gets singleton object.
 * @return singleton
 */
+ (MSSDK*)sharedInstance;

/**
 * Register mapping in managed object store
 * @param entityName Name of Class in Core Data
 * @param dictionary Mapping Dictionary
 * @param idenificationAttributes Identity Columns of Entity
 */
- (void)registerEntityMapping:(NSString *)entityName
            mappingDictionary:(NSDictionary *)dictionary
      identificationAttribute:(NSArray *)idenificationAttributes
                  pathPattern:(NSString *)pathPattern;

@end

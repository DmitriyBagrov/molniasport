//
//  MSSDK.m
//  molniaSport
//
//  Created by Dmitriy Bagrov on 09.10.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//

#import "MSSDK.h"

@interface MSSDK()

@property (nonatomic, strong) RKObjectManager* objectManager;
@property (nonatomic, strong) RKManagedObjectStore* managedObjectStore;

@end

@implementation MSSDK

static MSSDK *SINGLETON = nil;

static bool isFirstAccess = YES;

#pragma mark - Public Method

+ (id)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        isFirstAccess = NO;
        SINGLETON = [[super allocWithZone:NULL] init];    
    });
    
    return SINGLETON;
}

#pragma mark - Life Cycle

+ (id)allocWithZone:(NSZone *)zone {
    return [self sharedInstance];
}

+ (id)copyWithZone:(struct _NSZone *)zone {
    return [self sharedInstance];
}

+ (id)mutableCopyWithZone:(struct _NSZone *)zone {
    return [self sharedInstance];
}

- (id)copy {
    return [[MSSDK alloc] init];
}

- (id)mutableCopy {
    return [[MSSDK alloc] init];
}

- (id)init {
    if(SINGLETON){
        return SINGLETON;
    }
    if (isFirstAccess) {
        [self doesNotRecognizeSelector:_cmd];
    }
    self = [super init];
    [self setupStorage];
    return self;
}

#pragma mark - Setup CoreData and RestKit

- (void)setupStorage {
    self.objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:MSbaseUrl]];
    
    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    // Initialize managed object store
    self.managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    self.objectManager.managedObjectStore = self.managedObjectStore;
    
    // Complete Core Data stack initialization
    [self.managedObjectStore createPersistentStoreCoordinator];
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:MSstorePath];
    NSString *seedPath = [[NSBundle mainBundle] pathForResource:@"RKSeedDatabase" ofType:@"sqlite"];
    NSError *error;
    NSPersistentStore *persistentStore = [self.managedObjectStore addSQLitePersistentStoreAtPath:storePath
                                                                          fromSeedDatabaseAtPath:seedPath
                                                                               withConfiguration:nil
                                                                                         options:nil
                                                                                           error:&error];
    NSAssert(persistentStore, @"Failed to add persistent store with error: %@", error);
    
    
    // Create the managed object contexts
    [self.managedObjectStore createManagedObjectContexts];
    
    // Configure a managed object cache to ensure we do not create duplicate objects
    self.managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:self.managedObjectStore.persistentStoreManagedObjectContext];
}

#pragma mark - Register Entity in Object Store

- (void)registerEntityMapping:(NSString *)entityName
            mappingDictionary:(NSDictionary *)dictionary
      identificationAttribute:(NSArray *)idenificationAttributes
                  pathPattern:(NSString *)pathPattern {
    //RKEntityMapping Object Initialization
    RKEntityMapping *mapping = [RKEntityMapping mappingForEntityForName:entityName inManagedObjectStore:self.managedObjectStore];
    mapping.identificationAttributes = idenificationAttributes;
    [mapping addAttributeMappingsFromDictionary:dictionary];
    
    //RKResponse Descriptor for Mapping
    RKResponseDescriptor *descriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                    method:RKRequestMethodGET
                                                                               pathPattern:pathPattern
                                                                                   keyPath:nil
                                                                               statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    //Register Response Descriptor in Object Manager
    [self.objectManager addResponseDescriptor:descriptor];
}


@end

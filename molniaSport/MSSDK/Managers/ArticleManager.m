//
//  ArticleManager.m
//  molniaSport
//
//  Created by Dmitriy Bagrov on 10.10.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//

#import "ArticleManager.h"

@implementation ArticleManager

static ArticleManager *SINGLETON = nil;

static bool isFirstAccess = YES;

#pragma mark - Public Method

+ (id)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        isFirstAccess = NO;
        SINGLETON = [[super allocWithZone:NULL] init];
    });
    
    return SINGLETON;
}

#pragma mark - Life Cycle

+ (id) allocWithZone:(NSZone *)zone {
    return [self sharedInstance];
}

+ (id)copyWithZone:(struct _NSZone *)zone {
    return [self sharedInstance];
}

+ (id)mutableCopyWithZone:(struct _NSZone *)zone {
    return [self sharedInstance];
}

- (id)copy {
    return [[ArticleManager alloc] init];
}

- (id)mutableCopy {
    return [[ArticleManager alloc] init];
}

- (id) init {
    if(SINGLETON){
        return SINGLETON;
    }
    if (isFirstAccess) {
        [self doesNotRecognizeSelector:_cmd];
    }
    self = [super init];
    [self setup];
    return self;
}

#pragma mark - Setup Response Descriptor in Store

- (void)setup {
    //Setup Feed Article Descriptor
    [[MSSDK sharedInstance] registerEntityMapping:NSStringFromClass([Article class])
                                mappingDictionary:[Article mappingDictionary]
                          identificationAttribute:[Article identityAttributes]
                                      pathPattern:MSarticleListUrlPattern];
    
    //Setup Article Descriptor
    [[MSSDK sharedInstance] registerEntityMapping:NSStringFromClass([Article class])
                                mappingDictionary:[Article mappingDictionary]
                          identificationAttribute:[Article identityAttributes]
                                      pathPattern:MSarticleUrlPattern];
}

#pragma mark - Access Methods

- (void)articleListWithCount:(NSInteger)count
                      offset:(NSInteger)offset
                  completion:(void (^)(NSArray *, NSError *))completion {
    [[RKObjectManager sharedManager]
     getObjectsAtPath:[NSString stringWithFormat:MSarticleListUrl, count, offset]
     parameters:nil
     success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
         completion([self articlesFromStorageWithCount:count offset:offset], nil);
     } failure:^(RKObjectRequestOperation *operation, NSError *error) {
         completion([self articlesFromStorageWithCount:count offset:offset], error);
     }];
}

- (void)articleById:(NSString *)uid completion:(void (^)(Article *, NSError *))completion {
    [[RKObjectManager sharedManager]
     getObjectsAtPath:[NSString stringWithFormat:MSarticleUrl, uid]
           parameters:nil
              success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                completion([self articleFromStorageById:uid], nil);
            } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                completion([self articleFromStorageById:uid], error);
    }];
}

#pragma mark - Helper Storage Access Methods

- (NSArray *)articlesFromStorageWithCount:(NSInteger)count
                                   offset:(NSInteger)offset {
    NSManagedObjectContext *context = [RKManagedObjectStore defaultStore].mainQueueManagedObjectContext;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Article class])];
    fetchRequest.fetchLimit = count;
    fetchRequest.fetchOffset = offset;
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    return fetchedObjects;
}

- (Article *)articleFromStorageById:(NSString *)uid {
    NSManagedObjectContext *context = [RKManagedObjectStore defaultStore].mainQueueManagedObjectContext;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Article class])];
    NSPredicate *fetchPredicate = [NSPredicate predicateWithFormat:@"(uid == %@)",uid];
    [fetchRequest setPredicate:fetchPredicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    return [fetchedObjects firstObject];
}


@end

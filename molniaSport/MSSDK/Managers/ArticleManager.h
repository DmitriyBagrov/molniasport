//
//  ArticleManager.h
//  molniaSport
//
//  Created by Dmitriy Bagrov on 10.10.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//

#import "MSSDK.h"

@interface ArticleManager : NSObject

/**
 * gets singleton object.
 * @return singleton
 */
+ (ArticleManager*)sharedInstance;

/**
 * gets articles list
 * @param count Number of articles in completion
 * @param offset Offset from beginning
 * @param completion Completion with articles or error
 */
- (void)articleListWithCount:(NSInteger)count
                     offset:(NSInteger)offset
                 completion:(void (^)(NSArray *articles, NSError* error))completion;
/**
 * gets article detailed info
 * @param uid Unique identifier of article
 * @param completion Completion with article or error
 */
- (void)articleById:(NSString *)uid completion:(void (^)(Article *article, NSError* error))completion;

@end

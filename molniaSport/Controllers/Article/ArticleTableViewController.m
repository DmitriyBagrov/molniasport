//
//  ArticleTableViewController.m
//  molniaSport
//
//  Created by Dmitriy Bagrov on 10.10.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//
//Manager
#import "ArticleManager.h"

//Cells
#import "ArticleTitleTableViewCell.h"
#import "ArticleHeaderTableViewCell.h"
#import "ArticleContentTableViewCell.h"

#import "ArticleTableViewController.h"

@interface ArticleTableViewController ()

@end

@implementation ArticleTableViewController

#pragma mark - Life circle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureNavigationItems];
    [self loadAndDisplayDetailedArticleInfo];
}

#pragma mark - Configure method

- (void)configureNavigationItems {
    [self hideConnectionError];
}

#pragma mark - Load Method

- (void)loadAndDisplayDetailedArticleInfo {
    __weak typeof(self) welf = self;
    [[ArticleManager sharedInstance] articleById:self.article.uid
                                      completion:^(Article *article, NSError *error) {
                                          if (error) {
                                              [welf showConnectionError];
                                          } else {
                                              [welf hideConnectionError];
                                          }
                                          welf.article = article;
                                          [welf.tableView reloadData];
                                      }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        return [self cellForHeaderViewAtSection:section];
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        return ArticleHeaderTableViewCellHeight;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return [self cellForTitleRowAtIndexPath:indexPath];
        case 1:
            return [self cellForContentRowAtIndexPath:indexPath];
        default:
            //TODO: NSAssert
            return nil;
    }
}

#pragma mark - Table View Cells

- (UITableViewCell *)cellForTitleRowAtIndexPath:(NSIndexPath *)indexPath {
    ArticleTitleTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CIArticleTitleTableViewCell];
    [cell displayArticle:self.article];
    return cell;
}

- (UITableViewCell *)cellForHeaderViewAtSection:(NSInteger)section {
    ArticleHeaderTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CIArticleHeaderTableViewCell];
    [cell displayArticle:self.article];
    return cell;
}

- (UITableViewCell *)cellForContentRowAtIndexPath:(NSIndexPath *)indexPath {
    ArticleContentTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CIArticleContentTableViewCell];
    [cell displayArticle:self.article];
    return cell;
}

@end

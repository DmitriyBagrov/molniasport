//
//  ArticleContentTableViewCell.m
//  molniaSport
//
//  Created by Dmitriy Bagrov on 11.10.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//

#import "ArticleContentTableViewCell.h"

@interface ArticleContentTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end

@implementation ArticleContentTableViewCell

- (void)displayArticle:(Article *)article {
    self.subtitleLabel.text = article.subtitle;
    self.contentLabel.text = article.content;
    
    //Update constraint for auto-layout cell height calculation
    [self.contentLabel setNeedsUpdateConstraints];
    [self.contentLabel updateConstraintsIfNeeded];
}

@end

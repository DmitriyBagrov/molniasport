//
//  ArticleHeaderTableViewCell.m
//  molniaSport
//
//  Created by Dmitriy Bagrov on 11.10.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//

#import "ArticleHeaderTableViewCell.h"

@interface ArticleHeaderTableViewCell()

//IBOutlets: Views
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *hitsLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;

@end

@implementation ArticleHeaderTableViewCell

- (void)displayArticle:(Article *)article {
    self.titleLabel.text = article.title;
    
    self.hitsLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Просмотров %@", @""), article.hits];
    self.commentLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Комментариев %@", @""), article.commentsCount];
}

@end

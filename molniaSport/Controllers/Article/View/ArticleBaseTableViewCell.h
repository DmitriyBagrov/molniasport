//
//  ArticleBaseTableViewCell.h
//  molniaSport
//
//  Created by Dmitriy Bagrov on 11.10.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//

@interface ArticleBaseTableViewCell : UITableViewCell

- (void)displayArticle:(Article *)article;

@end

//
//  ArticleTitleTableViewCell.m
//  molniaSport
//
//  Created by Dmitriy Bagrov on 11.10.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//

#import "ArticleTitleTableViewCell.h"

@interface ArticleTitleTableViewCell()

//IBOutlets: Views
@property (weak, nonatomic) IBOutlet UIImageView *articleImageView;

@end

@implementation ArticleTitleTableViewCell

- (void)displayArticle:(Article *)article {
    if (article.imageUrl) {
        [self.articleImageView sd_setImageWithURL:[NSURL URLWithString:article.imageUrl]];
    } else if (article.smallImageUrl) {
        [self.articleImageView sd_setImageWithURL:[NSURL URLWithString:article.smallImageUrl]];
    }
}

@end

//
//  ArticleTableViewController.h
//  molniaSport
//
//  Created by Dmitriy Bagrov on 10.10.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//

#import "BaseTableViewController.h"

@interface ArticleTableViewController : BaseTableViewController

@property (nonatomic, strong) Article* article;

@end

//
//  FeedTableViewController.m
//  molniaSport
//
//  Created by Dmitriy Bagrov on 10.10.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//

//Manager
#import "ArticleManager.h"

//Cells
#import "FeedArticleTableViewCell.h"
#import "FeedLoadingTableViewCell.h"

//Controllers
#import "ArticleTableViewController.h"
#import "FeedTableViewController.h"

@interface FeedTableViewController ()

//Storage Properties
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) BOOL isEnd;
@property (nonatomic, strong) NSMutableArray* articles;

@end

@implementation FeedTableViewController

#pragma mark - Life circle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureRefreshControll];
    [self configureNavigationItems];
}

#pragma mark - Configure

- (void)configureRefreshControll {
    UIRefreshControl *refreshControll = [UIRefreshControl new];
    [refreshControll addTarget:self action:@selector(refreshFeed:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControll;
}

- (void)configureNavigationItems {
    [self hideConnectionError];
}

#pragma mark - Data Loading Methods

- (void)loadAndDisplayArticles:(UIRefreshControl *)sender {
    if (self.isLoading) {
        [sender endRefreshing];
        return;
    }
    if (!self.articles) {
        self.articles = [NSMutableArray array];
    }
    if (sender) {
        [self.articles removeAllObjects];
    }
    __weak typeof(self) welf = self;
    self.isLoading = YES;
    [[ArticleManager sharedInstance] articleListWithCount:kFeedArticlesInPage
                                                   offset:self.articles.count
                                               completion:^(NSArray *articles, NSError *error) {
                                                   if (error) {
                                                       [welf showConnectionError];
                                                   } else {
                                                       [welf hideConnectionError];
                                                   }
                                                   welf.isEnd = articles.count < kFeedArticlesInPage;
                                                   welf.isLoading = NO;
                                                   [welf.articles addObjectsFromArray:articles];
                                                   [sender endRefreshing];
                                                   [welf.tableView reloadData];
                                               }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isEnd) {
        return self.articles.count;
    } else {
        return self.articles.count + 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.articles.count) {
        return [self feedLoadingCellForRowAtIndexPath:indexPath];
    } else {
        return [self feedArticleCellForRowAtIndexPath:indexPath];
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isKindOfClass:[FeedLoadingTableViewCell class]]) {
        [self loadAndDisplayArticles:nil];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self performSegueWithIdentifier:kSIOpenArticleTableViewController
                              sender:[self.articles objectAtIndex:indexPath.row]];
}

#pragma mark - Table View Cells

- (UITableViewCell *)feedLoadingCellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FeedLoadingTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CIFeedLoadingTableViewCell];
    return cell;
}

- (UITableViewCell *)feedArticleCellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FeedArticleTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CIFeedArticleTableViewCell];
    [cell displayArticle:[self.articles objectAtIndex:indexPath.row]];
    return cell;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kSIOpenArticleTableViewController]) {
        [self prepareForArticleSegue:segue sender:sender];
    }
}

- (void)prepareForArticleSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ArticleTableViewController *vc = segue.destinationViewController;
    vc.article = sender;
}

#pragma mark - Refresh Action

- (void)refreshFeed:(UIRefreshControl *)sender {
    [self loadAndDisplayArticles:sender];
}

@end

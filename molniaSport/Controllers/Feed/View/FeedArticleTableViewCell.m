//
//  FeedArticleTableViewCell.m
//  molniaSport
//
//  Created by Dmitriy Bagrov on 10.10.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//

#import "FeedArticleTableViewCell.h"

@interface FeedArticleTableViewCell()

//IBOutlets: View's
@property (weak, nonatomic) IBOutlet UIImageView *articleImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;

@end

@implementation FeedArticleTableViewCell

#pragma mark - Life circle

- (void)awakeFromNib {
}

#pragma mark - Display method

- (void)displayArticle:(Article *)article {
    self.titleLabel.text = article.title;
    self.subtitleLabel.text = article.subtitle;
    [self.articleImageView sd_setImageWithURL:[NSURL URLWithString:article.smallImageUrl]];
    
    //Update constraint for auto-layout cell height calculation
    [self.subtitleLabel setNeedsUpdateConstraints];
    [self.subtitleLabel updateConstraintsIfNeeded];
}

@end

//
//  FeedArticleTableViewCell.h
//  molniaSport
//
//  Created by Dmitriy Bagrov on 10.10.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//

@interface FeedArticleTableViewCell : UITableViewCell

- (void)displayArticle:(Article *)article;

@end

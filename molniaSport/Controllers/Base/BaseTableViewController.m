//
//  BaseTableViewController.m
//  molniaSport
//
//  Created by Dmitriy Bagrov on 11.10.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//

#import "BaseTableViewController.h"

@interface BaseTableViewController ()

@end

@implementation BaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureTableViewAutoLayout];
}

#pragma mark - Configure method

- (void)configureTableViewAutoLayout {
    self.tableView.estimatedRowHeight = BaseEstimatedHeightTableViewCell;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

#pragma mark - Navigation Item

- (void)showConnectionError {
    self.navigationItem.rightBarButtonItem = self.connectionErrorItem;
}

- (void)hideConnectionError {
    self.navigationItem.rightBarButtonItem = nil;
}

@end

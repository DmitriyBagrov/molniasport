//
//  BaseTableViewController.h
//  molniaSport
//
//  Created by Dmitriy Bagrov on 11.10.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//

@interface BaseTableViewController : UITableViewController

//IBOutlets: Controls
// (Strong to delete and restore on nav item)
@property (strong, nonatomic) IBOutlet UIBarButtonItem *connectionErrorItem;

/**
 * Show Connection error button on right navigation items
 */
- (void)showConnectionError;

/**
* Hide Connection error button on right navigation items
*/
- (void)hideConnectionError;

@end

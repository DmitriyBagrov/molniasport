//
//  AppConstants.m
//  molniaSport
//
//  Created by Dmitriy Bagrov on 09.10.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//

#pragma mark - API Constants

NSString* const MSbaseUrl = @"http://molniasport.ru/api/v1/";

#pragma mark - API Article paths

NSString* const MSarticleListUrlPattern = @"posts";

NSString* const MSarticleListUrl = @"posts?limit=%zd&offset=%zd";

NSString* const MSarticleUrlPattern = @"posts/:id";

NSString* const MSarticleUrl = @"posts/%@";

#pragma mark - Core Data Constants

NSString* const MSstorePath = @"molniaSport.sqlite";

#pragma mark - Feed Table View Properties

NSInteger const kFeedArticlesInPage = 10;

#pragma mark - Cell Identifiers
#pragma mark - Feed

NSString* const CIFeedArticleTableViewCell = @"FeedArticleTableViewCell";

NSString* const CIFeedLoadingTableViewCell = @"FeedLoadingTableViewCell";

#pragma mark - Article

NSString* const CIArticleTitleTableViewCell = @"ArticleTitleTableViewCell";

NSString* const CIArticleHeaderTableViewCell = @"ArticleHeaderTableViewCell";

NSString* const CIArticleContentTableViewCell = @"ArticleContentTableViewCell";

#pragma mark - Cell Height
#pragma mark - Feed

CGFloat const BaseEstimatedHeightTableViewCell = 44.0;

CGFloat const ArticleHeaderTableViewCellHeight = 100.0;

#pragma mark - Segue Identifiers

NSString* const kSIOpenArticleTableViewController = @"OpenArticleTableViewController";


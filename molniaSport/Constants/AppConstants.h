//
//  AppConstants.h
//  molniaSport
//
//  Created by Dmitriy Bagrov on 09.10.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//

#pragma mark - API Constants

extern NSString* const MSbaseUrl;

#pragma mark - API Article

extern NSString* const MSarticleListUrlPattern;

extern NSString* const MSarticleListUrl;

extern NSString* const MSarticleUrlPattern;

extern NSString* const MSarticleUrl;

#pragma mark - Core Data Constants

extern NSString* const MSstorePath;

#pragma mark - Feed Table View Properties

extern NSInteger const kFeedArticlesInPage;

#pragma mark - Cell Identifiers
#pragma mark - Feed 

extern NSString* const CIFeedArticleTableViewCell;

extern NSString* const CIFeedLoadingTableViewCell;

#pragma mark - Article

extern NSString* const CIArticleTitleTableViewCell;

extern NSString* const CIArticleHeaderTableViewCell;

extern NSString* const CIArticleContentTableViewCell;

#pragma mark - Cell Height
#pragma mark - Feed

extern CGFloat const BaseEstimatedHeightTableViewCell;

extern CGFloat const ArticleHeaderTableViewCellHeight;

#pragma mark - Segue Identifiers

extern NSString* const kSIOpenArticleTableViewController;